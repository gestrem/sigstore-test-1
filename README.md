
# Secure images with sigstore


# local build

```
podman build -t localhost/nodejs-ubi:v4-nosign . 
````

# tag to send to Quay 
```
podman tag localhost/nodejs-ubi:v4-nosign quay.io/gestrem/image-registry1:v4-nosign
```

# push to Quay

```
podman push quay.io/gestrem/image-registry1:v4-nosign
```

# create sigstore signature and verify

## with skopeo

```
skopeo generate-sigstore-key --output-prefix cosign-jfrog
```

### sign & send to Artifactory with a sigstore signature 

`
registries.d
`
is the repository where you specify config for remote registries. In this case, jfrog.yaml can attach sigstore artifact to the images.

More details here : https://github.com/containers/image/blob/main/docs/containers-registries.d.5.md#individual-configuration-sections

```
skopeo copy --registries.d registries.d  --sign-by-sigstore-private-key=cosign-jfrog.private docker://quay.io/gestrem/image-registry1:v4-nosign docker://gestrem.jfrog.io/demo1-jfrog-oci-oci/localhost/artifact-nodejs:v2 
```
### verify signature with cosign

```
cosign verify --private-infrastructure=true --key cosign-jfrog.pub gestrem.jfrog.io/demo1-jfrog-oci-oci/localhost/artifact-nodejs:v2 
```

``
--private-infrastructure=true
`` parameter is useful to skip transparency logs check because skopeo copy didn't write a record in a Rekor instance. 


## with Cosign

## sign with cosign sigstore key
```
cosign generate-key-pair
```
### sign & send to Artifactory with a sigstore signature 
```
podman push  gestrem.jfrog.io/demo1-jfrog-oci-oci/localhost/artifact-nodejs:v2 
```
```
cosign sign -key cosign.key gestrem.jfrog.io/demo1-jfrog-oci-oci/localhost/artifact-nodejs:v2 -y
```
### verify signature with cosign
```
cosign verify --key cosign.pub gestrem.jfrog.io/demo1-jfrog-oci-oci/localhost/artifact-nodejs:v2
```

